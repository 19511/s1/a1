package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


//! command to run app ./mvnw spring-boot:run
@SpringBootApplication

@RestController //! tells spring boot that will use endpoints for handling web requests and responses.
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	@GetMapping("/hello")  //! /hello -default "Hello World /hello?name=value - will print "Hello <value added on the url> E.G localhost:8080/hello?name=Franz  => Hello Franz
	public String hello (@RequestParam (value = "name", defaultValue = "World")String name){
		return String.format("Hello %s", name);

		//! %s will take the second value [Template literals in JS]




	}
	//! Activity I - Spring-boot

	@GetMapping("/hi")
	public String hi (@RequestParam (value = "username", defaultValue = "user")String username){
		return "Hi " + username;
	}

}
